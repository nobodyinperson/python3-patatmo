# -*- coding: utf-8 -*-
# System modules

# External modules

# Internal modules

import patatmo.api.authentication
import patatmo.api.client
import patatmo.api.errors
import patatmo.api.variables
import patatmo.api.requests
import patatmo.api.responsetypes

__all__ = ['authentication']
