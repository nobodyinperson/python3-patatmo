
How to use :mod:`patatmo`
=========================

Creating a :any:`NetatmoClient`
+++++++++++++++++++++++++++++++

All :mod:`patatmo` does is querying the `Netatmo API
<https://dev.netatmo.com>`_. This is done via a :any:`NetatmoClient`:

.. code-block:: python

    import patatmo

    # your patatmo connect developer credentials
    credentials = {
        "password":"5uP3rP45sW0rD",
        "username":"user.email@internet.com",
        "client_id":    "03012823b3fd2e420fbf980b",
        "client_secret":"YXNkZmFzZGYgamFzamYgbGFzIG"
    }

    # create an api client
    client = patatmo.api.client.NetatmoClient()

    # tell the client's authentication your credentials
    client.authentication.credentials = credentials

    # optionally give the authentication a temporary file.
    # The tokens are then stored there for later reuse,
    # e.g. next time you invoke this script.
    # This saves time because no new tokens have to be requested.
    # New tokens are then only requested if the old ones expire.
    client.authentication.tmpfile = "temp_auth.json"


Have a look at the :doc:`examples` to learn what to do with a
:any:`NetatmoClient`.
