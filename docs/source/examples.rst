
Examples
========

The following is a demonstration of what can be achieved with a
:any:`NetatmoClient`. See :doc:`howto` for the setup of a :any:`NetatmoClient`.
In the examples, ``client`` is an instance of :any:`NetatmoClient`.

:any:`Getpublicdata`: Obtaining a Regional Snapshot
+++++++++++++++++++++++++++++++++++++++++++++++++++

The :any:`Getpublicdata` method can be used to obtain a **snapshot** of Netatmo
data in a specific **region**. This is what you can see on the `Netatmo
Weathermap <https://weathermap.netatmo.com>`__.

A simple :any:`Getpublicdata` example:

.. code-block:: python

    # lat/lon outline of Hamburg/Germany
    hamburg_region = {
        "lat_ne" : 53.7499,
        "lat_sw" : 53.3809,
        "lon_ne" : 10.3471,
        "lon_sw" : 9.7085,
    }

    # issue the API request
    hamburg = client.Getpublicdata(region = hamburg_region)
    # convert the response to a pandas.DataFrame
    print(hamburg.dataframe)


Output::

    index   altitude  humidity                 id   latitude  longitude  \
    0        0  30.000000        84  70:ee:50:12:9a:b8  53.516950  10.155990
    1        1  23.000000        83  70:ee:50:03:da:4c  53.523361  10.167193
    2        2  23.000000        76  70:ee:50:01:47:34  53.510080  10.165600
    3        3  15.000000        93  70:ee:50:03:bc:2c  53.530948  10.134062
    ..     ...        ...       ...                ...        ...        ...

         pressure  temperature       time_humidity       time_pressure  \
    0      1029.1          8.1 2017-02-16 10:59:31 2017-02-16 11:00:05
    1      1026.7          8.3 2017-02-16 10:53:53 2017-02-16 10:54:01
    2      1030.0          9.4 2017-02-16 10:53:06 2017-02-16 10:53:42
    3      1026.8          8.0 2017-02-16 10:56:32 2017-02-16 10:56:54
    ..        ...          ...                 ...                 ...

           time_temperature       timezone
    0   2017-02-16 10:59:31  Europe/Berlin
    1   2017-02-16 10:53:53  Europe/Berlin
    2   2017-02-16 10:53:06  Europe/Berlin
    3   2017-02-16 10:56:32  Europe/Berlin
    ..                  ...            ...

    [708 rows x 12 columns]

.. note::

   The ``id`` in the :any:`Getpublicdata` response is the ``device_id`` of the
   **indoor module**, **NOT** the ``module_id`` of the outdoor module. However,
   the response also contains the ``module_id`` s of the outdoor modules. In
   this example, you could access the ``module_id`` s by recursing into
   ``hamburg.response["body"]``.



:any:`Getmeasure`: Obtaining Timeseries
+++++++++++++++++++++++++++++++++++++++

The :any:`Getmeasure` method can be used to obtain **timeseries** data from a
given device.

A simple :any:`Getmeasure` example:

.. code-block:: python

    # retrieve data from the indoor module (leave the module_id out)
    indoor = client.Getmeasure(device_id = "70:ee:50:12:9a:b8")

    # retrieve data from the outdoor module (specify both ids)
    outdoor = client.Getmeasure(
        device_id = "70:ee:50:12:9a:b8", # example id
        module_id = "02:00:00:15:01:d0"  # example id
        )

    # the raw API response is accessible by the `response` attribute
    indoor.response
    outdoor.response

    # the response can be converted to a pandas.DataFrame by the `dataframe()`
    # method
    print(outdoor.dataframe())


Output::

                               Temperature   CO2  Humidity    ...    WindAngle GustStrength GustAngle
    time                                                      ...
    2016-12-12 16:28:42+00:00         24.7  None        44    ...         None         None      None
    2016-12-12 16:30:24+00:00         25.1  None        38    ...         None         None      None
    2016-12-12 16:32:29+00:00         21.1  None        48    ...         None         None      None
    2016-12-12 16:37:37+00:00         20.7  None        49    ...         None         None      None
    2016-12-12 16:41:53+00:00         20.5  None        50    ...         None         None      None

    [5 rows x 10 columns]


.. note::

   A common mistake is to forget to specify the ``module_id`` which most likely
   leads to the ``ApiResponseError: Device not found - Check Device ID and
   permissions`` if one is not the owner of the indoor module.
