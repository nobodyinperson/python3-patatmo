
Installation
============

:mod:`patatmo` is best installed via ``pip``::

    pip3 install --user patatmo
