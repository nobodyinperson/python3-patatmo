#!/usr/bin/env python3
# system modules
import unittest
import logging
import os
import shutil
import json
import time
import http.client
import pandas

# import authentication module
from patatmo.api import authentication
# import client module
from patatmo.api import client
# import test data
from .test_data import *
from .test_flow import *

# external modules
import numpy as np

# test settings
OFFLINE = False  # set to True to run only non-network tests

# skip everything
SKIPALL = False  # by default, don't skip everything

# get a logger
logger = logging.getLogger(__name__)


#############################
### test the Client class ###
#############################

# remove the TMPAUTHFILE
def removeTMPAUTHFILE():
    if os.path.exists(TMPAUTHFILE):
        logger.debug("removing TPMAUTHFILE '{}'".format(TMPAUTHFILE))
        os.remove(TMPAUTHFILE)

# get the JSON content of TMPAUTHFILE


def TMPAUTHFILEjson():
    try:  # open and read, return result
        with open(TMPAUTHFILE, "r") as f:
            return json.load(f)
    except BaseException:  # didn't work, return empty dict
        return {}

##################################
### base class for ClientTests ###
##################################


class ClientTest(BasicTest):
    # execute this before each test method
    def setUp(self):
        # unset the client
        self.client = None
        # make sure, there is no authfile in the beginning
        removeTMPAUTHFILE()

    # execute this after each test method
    def tearDown(self):
        # unset the client
        self.client = None
        # make sure, there is no authfile in the end
        removeTMPAUTHFILE()

    # check if Client object has all needed properties
    def client_has_all_needed_properties(self):
        # property classes
        types = {
            "authentication": authentication.Authentication,
        }
        # check for existence of properties
        # hasattr(self.client,prop) is not suitable because of the
        # (hasattr calls the property's getter somehow)
        for prop, cls in types.items():
            self.assertTrue(prop in dir(self.client))
            # Check that client.prop has correct class
            self.assertIsInstance(getattr(self.client, prop), cls)


####################################################
### test the different Client construction cases ###
####################################################
class ClientConstructorTest(ClientTest):
    # empty constructor test
    @testname("empty constructor")
    @unittest.skipIf(SKIPALL, "skipping all tests")
    def test_Empty(self):
        # initialize without any arguments
        self.client = client.NetatmoClient()
        # check properties
        self.client_has_all_needed_properties()

    @testname("constructor with authentication")
    @unittest.skipIf(SKIPALL, "skipping all tests")
    def test_OnlyEmptyAuthentication(self):
        # an authentication
        auth = authentication.Authentication()
        # initialize only with authentication
        self.client = client.NetatmoClient(authentication=auth)
        # attribute should be the same
        self.assertEqual(self.client.authentication, auth)

##########################################
### test the different Client requests ###
##########################################


class ClientRequestsTest(ClientTest):
    def setUp(self):
        # an authentication
        auth = authentication.Authentication()
        auth.credentials = REAL_CREDENTIALS
        auth.tmpfile = TMPAUTHFILE
        # a client
        self.client = client.NetatmoClient(authentication=auth)

    # execute this after each test method
    def tearDown(self):
        # unset the client
        self.client = None

    @classmethod
    def setUpClass(cls):
        # make sure, there is no authfile in the beginning
        removeTMPAUTHFILE()

    @classmethod
    def tearDownClass(cls):
        # make sure, there is no authfile in the end
        removeTMPAUTHFILE()

    # getpublicdata with correct region
    @testname("Getpublicdata with correct region")
    @unittest.skipIf(SKIPALL, "skipping all tests")
    def test_GetpublicdataWithHamburgRegion(self):
        if OFFLINE:
            self.skipTest("skip network dependant tests")
        # request with only hamburg coordinates
        res = self.client.Getpublicdata(region=HAMBURG_COORDINATES_OUTLINE)
        # as dataframe
        df_orig = res.dataframe()
        # without outliers
        df_inside = res.dataframe(only_inside=True)
        # check converted response class
        self.assertIsInstance(df_orig, pandas.DataFrame)
        self.assertIsInstance(df_inside, pandas.DataFrame)
        # original is always at least as big as the cut dataset
        self.assertTrue(df_orig.shape[0] >= df_inside.shape[0])
        lat_ne = HAMBURG_COORDINATES_OUTLINE["lat_ne"]
        lon_ne = HAMBURG_COORDINATES_OUTLINE["lon_ne"]
        lat_sw = HAMBURG_COORDINATES_OUTLINE["lat_sw"]
        lon_sw = HAMBURG_COORDINATES_OUTLINE["lon_sw"]
        inside = np.logical_and(
            df_inside["latitude"] <= lat_ne,
            np.logical_and(
                df_inside["latitude"] >= lat_sw,
                np.logical_and(
                    df_inside["longitude"] <= lon_ne,
                    df_inside["longitude"] >= lon_sw)))
        # in the cut dataset, all stations should be inside
        self.assertTrue(np.all(inside))

    # getmeasure with correct data
    @testname("Getmeasure with correct data and only device_id")
    @unittest.skipIf(SKIPALL, "skipping all tests")
    def test_GetmeasureWithDevice(self):
        if OFFLINE:
            self.skipTest("skip network dependant tests")
        DEVICE_ID = DEVICES.get("device_id")
        if not DEVICE_ID:
            self.skipTest("user device id missing "
                          "in {} and environment".format(USER_DATA_JSONFILE))
        # request
        res = self.client.Getmeasure(
            device_id=DEVICE_ID,
            optimize=False
        )
        # check converted response class
        self.assertIsInstance(res.dataframe(), pandas.DataFrame)

    # getmeasure with correct data
    @testname("Getmeasure with correct data and device_id and module_id")
    @unittest.skipIf(SKIPALL, "skipping all tests")
    def test_GetmeasureWithDeviceAndModule(self):
        if OFFLINE:
            self.skipTest("skip network dependant tests")
        DEVICE_ID = DEVICES.get("device_id")
        if not DEVICE_ID:
            self.skipTest("user device id missing "
                          "in {} and environment".format(USER_DATA_JSONFILE))
        MODULE_ID = DEVICES.get("module_id")
        if not MODULE_ID:
            self.skipTest("user module id missing "
                          "in {} and environment".format(USER_DATA_JSONFILE))
        # request
        res = self.client.Getmeasure(
            device_id=DEVICE_ID,
            module_id=MODULE_ID,
            optimize=False
        )
        # check converted response class
        self.assertIsInstance(res.dataframe(), pandas.DataFrame)

    # getmeasure with correct data but 0 time frame
    @testname("Getmeasure with device_id and module_id but no time")
    @unittest.skipIf(SKIPALL, "skipping all tests")
    def test_GetmeasureWithDeviceAndModuleNoTime(self):
        if OFFLINE:
            self.skipTest("skip network dependant tests")
        DEVICE_ID = DEVICES.get("device_id")
        if not DEVICE_ID:
            self.skipTest("user device id missing "
                          "in {} and environment".format(USER_DATA_JSONFILE))
        MODULE_ID = DEVICES.get("module_id")
        if not MODULE_ID:
            self.skipTest("user module id missing "
                          "in {} and environment".format(USER_DATA_JSONFILE))
        # request
        res = self.client.Getmeasure(
            device_id=DEVICE_ID,
            module_id=MODULE_ID,
            date_begin=1481799600,  # same time for
            date_end=1481799600,  # begin and end
            optimize=False
        )
        df = res.dataframe()
        # check converted response class
        self.assertIsInstance(df, pandas.DataFrame)
        # check that there really are now rows
        self.assertEqual(df.shape[0], 0)

    # getmeasure with correct data
    @testname("Getstationsdata with correct data")
    @unittest.skipIf(SKIPALL, "skipping all tests")
    def test_GetstationsdataWithDevice(self):
        if OFFLINE:
            self.skipTest("skip network dependant tests")
        DEVICE_ID = DEVICES.get("device_id")
        if not DEVICE_ID:
            self.skipTest("user device id missing "
                          "in {} and environment".format(USER_DATA_JSONFILE))
        # request with only hamburg coordinates
        res = self.client.Getstationsdata(
            device_id=DEVICE_ID,
        )
        # check converted response class
        self.assertIsInstance(res.dataframe(), pandas.DataFrame)


def run():
    # run the tests
    logger.info("=== CLIENT TESTS ===")
    unittest.main(exit=False, module=__name__)
    logger.info("=== END OF CLIENT TESTS ===")
